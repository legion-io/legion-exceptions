# Legion::Exceptions

A simple gem to hold all of the different custom Legion::Exceptions.  
 
Typically used by the [LegionIO](https://rubygems.org/gems/legionio) gem
