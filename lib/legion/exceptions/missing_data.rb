# frozen_string_literal: true

module Legion
  module Exception
    class MissingData < RuntimeError
    end
  end
end
