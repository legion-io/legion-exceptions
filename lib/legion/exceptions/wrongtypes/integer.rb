# frozen_string_literal: true

module Legion
  module Exception
    module WrongType
      class Integer < TypeError
      end
    end
  end
end
