# frozen_string_literal: true

module Legion
  module Exception
    class InvalidJson < StandardError; end
  end
end
