# frozen_string_literal: true

module Legion
  module Exception
    class MissingCache < RuntimeError
    end
  end
end
