# frozen_string_literal: true

module Legion
  module Exception
    class MissingArgument < StandardError
    end
  end
end
