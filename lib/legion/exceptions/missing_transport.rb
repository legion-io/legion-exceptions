# frozen_string_literal: true

module Legion
  module Exception
    class MissingTransport < RuntimeError
    end
  end
end
