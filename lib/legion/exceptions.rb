# frozen_string_literal: true

require 'legion/exceptions/version'
require 'legion/exceptions/handled_task'
require 'legion/exceptions/invalidjson'
require 'legion/exceptions/missing_argument'
require 'legion/exceptions/wrongtype'
require 'legion/exceptions/missing_cache'
require 'legion/exceptions/missing_data'
require 'legion/exceptions/missing_transport'

module Legion
  module Exceptions
  end
end
