# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'legion/exceptions/version'

Gem::Specification.new do |spec|
  spec.name          = 'legion-exceptions'
  spec.version       = Legion::Exceptions::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'Used to keep legion exceptions in one place'
  spec.description   = 'All of the different Legion Exceptions'
  spec.homepage      = 'https://bitbucket.org/legion-io/legion-exceptions'
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/legion-io/legion-exceptions'
  spec.metadata['documentation_uri'] = 'https://legionio.atlassian.net/wiki/spaces/LEGION/overview'
  spec.metadata['bug_tracker_uri'] = 'https://legionio.atlassian.net/jira/software/c/projects/EXCEPTIONS'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 2.5.0'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ['lib']
end
