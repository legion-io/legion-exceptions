# frozen_string_literal: true

require 'spec_helper'
require 'legion/exceptions/missing_transport'

RSpec.describe Legion::Exception::MissingTransport do
  it { is_expected.to be_a_kind_of RuntimeError }
end
