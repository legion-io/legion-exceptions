# frozen_string_literal: true

require 'spec_helper'
require 'legion/exceptions/handled_task'

RSpec.describe Legion::Exception::HandledTask do
  it { is_expected.to be_a_kind_of StandardError }
end
