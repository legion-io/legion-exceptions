# frozen_string_literal: true

require 'spec_helper'
require 'legion/exceptions/wrongtype'

RSpec.describe Legion::Exception::WrongType::Integer do
  it { is_expected.to be_a_kind_of TypeError }
end
