# frozen_string_literal: true

require 'spec_helper'
require 'legion/exceptions/version'

RSpec.describe Legion::Exceptions::VERSION do
  it { should_not be_nil }
  it { should be_a String }
end
