# frozen_string_literal: true

require 'spec_helper'
require 'legion/exceptions/invalidjson'

RSpec.describe Legion::Exception::InvalidJson do
  it { is_expected.to be_a_kind_of StandardError }
end
