# frozen_string_literal: true

require 'spec_helper'
require 'legion/exceptions/missing_argument'

RSpec.describe Legion::Exception::MissingArgument do
  it { is_expected.to be_a_kind_of StandardError }
end
