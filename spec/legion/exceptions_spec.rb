# frozen_string_literal: true

require 'spec_helper'
require 'legion/exceptions'
require 'legion/exceptions/version'

RSpec.describe Legion::Exceptions do
  it 'has a version number' do
    expect(Legion::Exceptions::VERSION).not_to be nil
  end
end

RSpec.describe Legion::Exceptions::VERSION do
  it { should_not be_nil }
end
